#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD || !process.env.EMAIL) {
    console.log('USERNAME, EMAIL and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TEST_TIMEOUT, 10) || 30000;

    let browser, app;
    let athenticated_by_oidc = false;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username, password) {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(4000);
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@type="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//form')).submit();
        await browser.wait(until.elementLocated(By.xpath('//h2[contains(text(), "All Connections")]')), TEST_TIMEOUT);
    }

    async function loginOIDC(username, password) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.xpath('//a[@href="api/ext/openid/login"]'));
        await browser.findElement(By.xpath('//a[@href="api/ext/openid/login"]')).click();

        if (!athenticated_by_oidc) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();

            athenticated_by_oidc = true;
        }

        await browser.wait(until.elementLocated(By.xpath('//h2[contains(text(), "All Connections")]')), TEST_TIMEOUT);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(4000);
        await browser.findElement(By.xpath('//div[contains(@class, "menu-title")]')).click();
        await browser.wait(until.elementLocated(By.xpath('//a[contains(text(), "Logout")]')), TEST_TIMEOUT);
        await browser.findElement(By.xpath('//a[contains(text(), "Logout")]')).click();
        await browser.sleep(2000);
        await browser.get('https://' + app.fqdn); // newer versions show a re-login screen...
        await browser.sleep(2000);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="login"]')), TEST_TIMEOUT);
    }

    async function createGroup() {
        await browser.get('https://' + app.fqdn + '/#/manage/mysql/userGroups/');
        await browser.sleep(4000);
        await browser.findElement(By.xpath('//th[text()="Group name:"]/following-sibling::td/input')).sendKeys('CloudronGroup');
        await browser.findElement(By.xpath('//button[text()="Save"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//span[text()="CloudronGroup"]')), TEST_TIMEOUT);
    }

    async function checkGroup() {
        await browser.get('https://' + app.fqdn + '/#/settings/userGroups');
        await browser.wait(until.elementLocated(By.xpath('//span[text()="CloudronGroup"]')), TEST_TIMEOUT);
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app (no sso)', function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, 'guacadmin', 'guacadmin'));
    it('can create group', createGroup);
    it('check group', checkGroup);
    it('can logout', logout);
    it('uninstall app (no sso)', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app (sso)', function () {
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, 'guacadmin', 'guacadmin'));
    it('can create group', createGroup);
    it('check group', checkGroup);
    it('can logout', logout);

    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can admin login', login.bind(null, 'guacadmin', 'guacadmin'));
    it('check group', checkGroup);
    it('can logout', logout);
    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('can restart app', function () {
        execSync('cloudron restart --app ' + app.id);
    });

    it('can admin login', login.bind(null, 'guacadmin', 'guacadmin'));
    it('check group', checkGroup);
    it('can logout', logout);
    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('move to different location', function (done) {
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        getAppInfo();
        done();
    });

    it('can admin login', login.bind(null, 'guacadmin', 'guacadmin'));
    it('check group', checkGroup);
    it('can logout', logout);
    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --appstore-id org.apache.guacamole.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, 'guacadmin', 'guacadmin'));
    it('can create group', createGroup);
    it('can logout', logout);

    it('can update', function () {
        execSync('cloudron update --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can admin login', login.bind(null, 'guacadmin', 'guacadmin'));
    it('check group', checkGroup);
    it('can logout', logout);
    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
