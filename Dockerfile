FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

# dependencies are listed here https://guacamole.apache.org/doc/1.2.0/gug/installing-guacamole.html
RUN apt-get update \
    && apt-get -y install libcairo2-dev libjpeg-turbo8-dev libpng-dev libossp-uuid-dev \
       libavcodec-dev libavformat-dev libavutil-dev libswscale-dev freerdp2-dev libfreerdp-client2-2 libpango1.0-dev libssh2-1-dev \
       libtelnet-dev libvncserver-dev libpulse-dev libssl-dev libvorbis-dev libwebp-dev libwebsockets-dev \ 
       tomcat9 libtcnative-1 ghostscript \
    && rm -rf /var/cache/apt /var/lib/apt/lists

# renovate: datasource=github-tags depName=apache/guacamole-server versioning=semver
ENV GUACAMOLE_VERSION=1.5.5

# this env var is used by guacamole web client
ENV GUACAMOLE_HOME /app/code/guacamole-client-home
ARG DOWNLOAD_URL="http://apache.org/dyn/closer.cgi?action=download&filename=guacamole/${GUACAMOLE_VERSION}"

RUN mkdir -p "${GUACAMOLE_HOME}"
WORKDIR /app/code

# Compile the server (gaucd)
# guaclog is an interpreter which accepts Guacamole protocol dumps
# this will install freerdp2 plugins in /usr/lib/x86_64-linux-gnu/freerdp2 (and thus the ldconfig)
RUN curl -L "${DOWNLOAD_URL}/source/guacamole-server-${GUACAMOLE_VERSION}.tar.gz" | tar -zx \
    && cd guacamole-server-${GUACAMOLE_VERSION} \
    && ./configure --with-freerdp --with-telnet --with-ssh --with-vnc --with-websockets --with-webp --with-libavcodec --with-libavutil \
    && make \
    && make install \
    && cd .. \
    && rm -rf guacamole-server-${GUACAMOLE_VERSION} \
    && ldconfig

# Download extensions
RUN mkdir -p /app/code/extensions \
    && curl -L "${DOWNLOAD_URL}/binary/guacamole-auth-sso-${GUACAMOLE_VERSION}.tar.gz" | tar -xz \
    && curl -L "${DOWNLOAD_URL}/binary/guacamole-auth-jdbc-${GUACAMOLE_VERSION}.tar.gz" | tar -xz \
    && mv guacamole-auth-sso-${GUACAMOLE_VERSION}/openid/guacamole-auth-sso-openid-${GUACAMOLE_VERSION}.jar /app/code/extensions \
    && mv guacamole-auth-jdbc-${GUACAMOLE_VERSION}/mysql/guacamole-auth-jdbc-mysql-${GUACAMOLE_VERSION}.jar /app/code/extensions \
    && mv guacamole-auth-jdbc-${GUACAMOLE_VERSION}/mysql/schema /app/code/schema \
    && rm -rf guacamole-auth-sso-${GUACAMOLE_VERSION} guacamole-auth-jdbc-${GUACAMOLE_VERSION}

RUN ln -s /app/data/extensions "${GUACAMOLE_HOME}/extensions"

# Download MySQL connector https://dev.mysql.com/downloads/connector/j/
ARG MYSQL_CONNECTOR_VERSION=9.1.0
RUN mkdir -p "${GUACAMOLE_HOME}/lib" \
    && curl -L https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.tar.gz | tar -xz \
    && mv mysql-connector-j-${MYSQL_CONNECTOR_VERSION}/mysql-connector-j-${MYSQL_CONNECTOR_VERSION}.jar "${GUACAMOLE_HOME}/lib" \
    && rm -rf mysql-connector-j-${MYSQL_CONNECTOR_VERSION}

# Prepare tomcat. CATALINA_HOME is where tomcat was installed. and CATALINA_BASE is where the webapps to run are located
ENV CATALINA_HOME=/usr/share/tomcat9
ENV CATALINA_BASE=/app/code/tomcat9-base

RUN /usr/share/tomcat9/bin/makebase.sh "${CATALINA_BASE}" \
    && rm -rf ${CATALINA_BASE}/temp && ln -s /run/tomcat/temp ${CATALINA_BASE}/temp \
    && rm -rf ${CATALINA_BASE}/logs && ln -s /run/tomcat/logs ${CATALINA_BASE}/logs \
    && rm -rf ${CATALINA_BASE}/work && ln -s /run/tomcat/work ${CATALINA_BASE}/work \
    && rm -rf ${CATALINA_BASE}/conf && ln -s /etc/tomcat9 ${CATALINA_BASE}/conf

# https://guacamole.apache.org/doc/gug/proxying-guacamole.html#tomcat-remote-ip
RUN xmlstarlet ed --inplace --subnode "/Server/Service/Engine/Host" --type elem -n Valve /etc/tomcat9/server.xml && \
    xmlstarlet ed --inplace \
        --insert "/Server/Service/Engine/Host/Valve[last()]" --type attr -n className -v "org.apache.catalina.valves.RemoteIpValve" \
        --insert "/Server/Service/Engine/Host/Valve[last()]" --type attr -n internalProxies -v "172.18.0.1" \
        --insert "/Server/Service/Engine/Host/Valve[last()]" --type attr -n remoteIpHeader -v "x-forwarded-for" \
        --insert "/Server/Service/Engine/Host/Valve[last()]" --type attr -n remoteIpProxiesHeader -v "x-forwarded-by" \
        --insert "/Server/Service/Engine/Host/Valve[last()]" --type attr -n protocolHeader -v "x-forwarded-proto" \
        /etc/tomcat9/server.xml

# Get WAR app (the web client)
RUN wget "$DOWNLOAD_URL/binary/guacamole-${GUACAMOLE_VERSION}.war" -O /tmp/guacamole.war \
    && unzip /tmp/guacamole.war -d "${CATALINA_BASE}/webapps/ROOT" \
    && rm /tmp/guacamole.war

# there can also be a logback.xml (https://guacamole.apache.org/doc/gug/configuring-guacamole.html)
RUN ln -s /run/guacamole/guacamole.properties "${GUACAMOLE_HOME}/guacamole.properties"

# add supervisor configs
ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/guacamole/supervisord.log /var/log/supervisor/supervisord.log

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
