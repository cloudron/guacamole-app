This app is pre-setup with an admin account. The initial credentials are:

**Username**: guacadmin<br/>
**Password**: guacadmin<br/>

<sso>
By default, Cloudron users have regular users permissions. The user permissions can be changed on the users management page.
</sso>
