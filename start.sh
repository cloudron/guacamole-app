#!/bin/bash

set -eu

readonly mysql="mysql -u ${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} -h ${CLOUDRON_MYSQL_HOST} --port ${CLOUDRON_MYSQL_PORT} --database ${CLOUDRON_MYSQL_DATABASE}"

echo "==> Creating directories"
mkdir -p /run/guacamole /run/tomcat/temp /run/tomcat/logs /run/tomcat/work /app/data/guacd-home /app/data/extensions

cat > /run/guacamole/guacamole.properties <<EOF
guacd-hostname: 127.0.0.1
guacd-port:     4822
mysql-auto-create-accounts: true
mysql-hostname:${CLOUDRON_MYSQL_HOST}
mysql-port:${CLOUDRON_MYSQL_PORT}
mysql-database:${CLOUDRON_MYSQL_DATABASE}
mysql-username:${CLOUDRON_MYSQL_USERNAME}
mysql-password:${CLOUDRON_MYSQL_PASSWORD}
enable-clipboard-integration: true

EOF

echo "==> enable database extension"
rm -f /app/data/extensions/guacamole-auth-jdbc-mysql-${GUACAMOLE_VERSION}.jar
ln -s /app/code/extensions/guacamole-auth-jdbc-mysql-${GUACAMOLE_VERSION}.jar /app/data/extensions/guacamole-auth-jdbc-mysql-${GUACAMOLE_VERSION}.jar

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "==> enabling OIDC"

# CLOUDRON_OIDC_PROVIDER_NAME is not supported
# https://github.com/apache/guacamole-manual/blob/main/src/openid-auth.md
    cat >> /run/guacamole/guacamole.properties <<EOF
openid-id: Cloudron
openid-issuer: ${CLOUDRON_OIDC_ISSUER}
openid-authorization-endpoint: ${CLOUDRON_OIDC_AUTH_ENDPOINT}
openid-jwks-endpoint: ${CLOUDRON_OIDC_KEYS_ENDPOINT}
openid-client-id: ${CLOUDRON_OIDC_CLIENT_ID}
openid-client-secret: "${CLOUDRON_OIDC_CLIENT_SECRET}"
openid-redirect-uri: ${CLOUDRON_APP_ORIGIN}/
openid-username-claim-type: preferred_username
extension-priority: *, openid
openid-scope: openid email profile groups
EOF

    rm -f /app/data/extensions/guacamole-auth-sso-openid-${GUACAMOLE_VERSION}.jar
    ln -s /app/code/extensions/guacamole-auth-sso-openid-${GUACAMOLE_VERSION}.jar /app/data/extensions/guacamole-auth-sso-openid-${GUACAMOLE_VERSION}.jar
fi

[[ ! -f /app/data/guacamole.properties ]] && echo -e "# Add custom properties here. Be sure to restart the app after you make changes to this file\n" > /app/data/guacamole.properties

# append user properties
cat /app/data/guacamole.properties >> /run/guacamole/guacamole.properties

if [[ ! -f /app/data/.database-version ]]; then
    echo "==> init database on first run"
    cat /app/code/schema/*.sql | $mysql
    echo "${GUACAMOLE_VERSION}" > /app/data/.database-version
else
   if [[ "$(cat /app/data/.database-version)" != "${GUACAMOLE_VERSION}" ]]; then
        echo "==> updating database"
        cat /app/code/schema/upgrade/upgrade-pre-${GUACAMOLE_VERSION}.sql | $mysql
        echo "${GUACAMOLE_VERSION}" > /app/data/.database-version
    fi
fi

echo "==> Changing ownership"
chown -R tomcat:tomcat /run/tomcat /run/guacamole
chown -R cloudron:cloudron /app/data/guacd-home

echo "==> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Guacamole

